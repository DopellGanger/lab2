import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.nio.charset.Charset;

public class Lab2 {
  public static void main(String[] args) {
    String inputFilename = "i.txt", outputFilename = "o.txt";
    Scanner in;
    Path out = Paths.get(outputFilename);

    try {
      in = new Scanner(new File(inputFilename));
    } catch (FileNotFoundException e) {
      System.out.println("File " + inputFilename + " not found");
      return;
    }

    ArrayList<String> strs = new ArrayList<>();

    while (in.hasNextLine()) {
      Point3d[] pts3 = getPts3(in.nextLine());
      strs.add(String.format("%.15f", area(pts3)));
    }

    try {
      Files.write(out, strs, Charset.forName("UTF-8"));
    } catch (IOException e) {
      System.out.println("Can't write to " + outputFilename);
    }
  }

  private static Point3d[] getPts3(String str) {
    String[] pts3Str = str.split("\\s+\\|\\s+");
    double[] coords;
    Point3d[] pts3 = new Point3d[3];
    for (int i = 0; i < pts3Str.length; i++) {
      coords = getDCoords(pts3Str[i]);
      pts3[i] = new Point3d(coords[0], coords[1], coords[2]);
    }
    return pts3;
  }

  private static double[] getDCoords(String tochka) {
    String[] strCoor = tochka.split("\\s+");
    double[] dCoords = new double[3];
    for (int i = 0; i < strCoor.length; i++)
      dCoords[i] = Double.parseDouble(strCoor[i]);
    return dCoords;
  }

  private static double area(Point3d[] pts) {
    double a = pts[0].distanceTo(pts[1]);
    double b = pts[1].distanceTo(pts[2]);
    double c = pts[2].distanceTo(pts[0]);
    double p = (a + b + c) / 2;
    double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
    return s;
  }
}
